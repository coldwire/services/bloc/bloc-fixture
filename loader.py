import pprint
import random
import requests
import faker
from entity import UserEntity, DirectoryEntity


class Fixture:

    def __init__(self, domain: str, iteration: int = 3) -> None:
        self.domain     = domain
        self.iteration  = iteration
        self.fake       = faker.Faker()
        self.users: list[UserEntity] = []
        self.directories: dict = {}

    def load_users(self):
        for x in range(self.iteration):
            current_username: str = self.fake.user_name()
            current_password: str = self.fake.password(length=16)
            res_register = requests.post(f'{self.domain}/auth/register', json={
                "username": current_username,
                "password": current_password,
                "public_key": self.fake.password(length=45, special_chars=False, upper_case=False),
                "private_key": self.fake.password(length=45, special_chars=False, upper_case=False)
            })

            res_login = requests.post(f'{self.domain}/auth/login', json={
                "username": current_username,
                "password": current_password
            })

            if res_register.status_code >= 200 and res_register.status_code < 300 and res_login.status_code >= 200 and res_login.status_code < 300:
                current_root = res_login.json()["content"]["root"]
                current_token = res_login.headers.get("Set-Cookie").split(";")[0].split("=")[1]
                self.users.append(UserEntity(
                    current_username,
                    current_password,
                    current_root,
                    current_token
                ))
            else:
                print("Failed request")

    def load_directories(self):
        for user in self.users:
            self.directories[user.username]: list = []
            for x in range(random.randint(4, 10)):
                res_bloc = requests.post(f'{self.domain}/blocs/create', json={
                    "name": self.fake.file_path(depth=4).split("/")[1],
                    "parent": user.root
                }, cookies={"token": user.token})
                bloc_data = res_bloc.json()["content"]
                if res_bloc.status_code >= 200 and res_bloc.status_code < 300:
                    print(f'Bloc added for {user.username}')
                    self.directories[user.username].append(DirectoryEntity(bloc_data["id"], user.root))
                else:
                    print(f'Bloc failed for {user.username}')
        pprint.pprint(self.directories)
