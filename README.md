# Bloc Fixture Loader

## Quickstart

```sh
$ git clone https://codeberg.org/coldwire/bloc-fixture.git
(bloc-fixture) $ python -m venv venv
(bloc-fixture) $ source ./venv/bin/activate         # WIN : .\venv\Scripts\Activate.(ps1/cmd)
(bloc-fixture) $ pip install -r ./requirements.txt
(bloc-fixture) $ python ./bloc-fixture.py -h
```