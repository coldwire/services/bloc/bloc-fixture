import argparse
from faker import Faker
from loader import Fixture

parser = argparse.ArgumentParser(
    prog="bloc-fixture.py",
    usage='python %(prog)s [options]',
    description="Bloc CLI to load fake data fixtures"
)
parser.add_argument(
    '-d',
    '--domain',
    type=str,
    required=True,
    help="this can be 'https://localhost:8080' or 'https://example.com'"
)

parser.add_argument(
    '-i',
    '--iteration',
    type=int,
    default=2,
    help="Number of users generated"
)

args = parser.parse_args()


if __name__ == "__main__":
    fixture = Fixture(args.domain, iteration=args.iteration)
    fixture.load_users()
    fixture.load_directories()

